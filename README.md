# Lineare Algebra I

Hier ist Material zur Vorlesung 'Lineare Algebra I' an der Universität
Augsburg im Sommersemester 2019 zu finden.
Die
[Übungsblätter](http://algebra-und-zahlentheorie.gitlab.io/lineare-algebra/uebungen.pdf)
zur Vorlesung stehen als pdf zum Download bereit.
